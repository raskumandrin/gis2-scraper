#!/usr/bin/perl -w

#
# 2gis onliner parser 0.1.4 / 18.08.2012
#

use strict;
use warnings;

use JSON;
use MIME::Base64;
use LWP::UserAgent;
use Encode qw/encode decode/;
use encoding 'utf8';
use encoding 'utf8', STDOUT => 'cp866';
use Data::Dumper;

####################################################################################################
my @city_list = split(/,/, decode_base64(join"", <DATA>));
my @header = ('Наименование','Рубрика','Подрубрика','Адрес',
  'Дополнительно','Телефон','Факс','Время работы',
  'Сайт','E-mail','Долгота','Широта','Город');
my %week = (
  San => ['Вс', 0],
  Mon => ['Пн', 0],
  Tue => ['Вт', 0],
  Wed => ['Ср', 0],
  Thu => ['Чт', 0],
  Fri => ['Пт', 0],
  Sat => ['Сб', 0]
);
####################################################################################################
sub get
{
  my $ua = LWP::UserAgent->new();
  $ua->default_header(referer => 'http://maps.2gis.ru');
  my $res = $ua->get(shift);
  return $$res{_content};
}
#
sub get_rubs
{
  my ( $cid, %h ) = shift;
  my $jsn = get(qq~http://catalog.api.2gis.ru/rubricator?show_children=1&project_id=$cid&output=jsonp&key=ruokep8523&version=1.3&lang=ru&callback=DG.Online.Utils.Ajax.callback.dga_3~);
  $jsn =~ s/.*?\(//;
  $jsn =~ s/\)$//;
  my $jref = decode_json( $jsn );

  foreach my $k1 ( @{$$jref{result}} )
  {
    $h{ $k1->{id} }{name} = $k1->{name};
    $h{ $k1->{id} }{rubs} = [];
    foreach my $k2 ( @{$k1->{children}} )
    {
      push @{$h{ $k1->{id} }{rubs}}, [ $k2->{id}, $k2->{name} ]; 
    } 
  }
  return \%h;
}
#
sub srub_parsing
{
	my ( $jsn, %db ) = shift;
  $jsn =~ s/.*?\(//;
  $jsn =~ s/\)$//;
	eval { $jsn = decode_json( $jsn ) };
	
	if($$jsn{response_code} == 400 || $$jsn{results}{firm}{total} == 0 || $@)
	#if($$jsn{response_code} == 400 || $@)
	{
		return (400, \%db);
	}

  #open F, ">>/tmp/dump"; print F scalar(Dumper $jsn),"\n\n"; close F;

	for(my $i = 0; $i <= scalar(@{$jsn->{results}{firm}{results}})-1; $i++)
	{
		my $name = $jsn->{results}{firm}{results}->[$i]->{name};
		my $city = $jsn->{results}{firm}{results}->[$i]->{city_name};
		my $addr = $jsn->{results}{firm}{results}->[$i]->{geometry_name};
		
		$db{$name}{city} = $city;
		$db{$name}{addr} = $addr;
    if( ref($$jsn{results}{firm}{results}->[$i]->{additional_info}) eq 'HASH')
    {
      $db{$name}{info} = ($$jsn{results}{firm}{results}->[$i]->{additional_info}{office} || '');
    }
    else
    {
      $db{$name}{info} = '';
    }
    $db{$name}{lon}  = $jsn->{results}{firm}{results}->[$i]->{lon};
    $db{$name}{lat}  = $jsn->{results}{firm}{results}->[$i]->{lat};
    $db{$name}{whs_comment} = $jsn->{results}{firm}{results}->[$i]->{workhours_comment};

    my ( %wk, $time ) = %week;

    if( ref($jsn->{results}{firm}{results}->[$i]->{whs}{workhours}) eq 'HASH')
    {
      foreach my $day (keys %{$jsn->{results}{firm}{results}->[$i]->{whs}{workhours}})
      {
        $wk{$day}[1] = 1;
        if( $jsn->{results}{firm}{results}->[$i]->{whs}{workhours}{$day}{'working_hours-0'} && $jsn->{results}{firm}{results}->[$i]->{whs}{workhours}{$day}{'working_hours-1'} )
        {
          $time = sprintf('%s-%s, %s-%s, Выходные: ',
            $jsn->{results}{firm}{results}->[$i]->{whs}{workhours}{$day}{'working_hours-0'}{from},
            $jsn->{results}{firm}{results}->[$i]->{whs}{workhours}{$day}{'working_hours-1'}{to},
            $jsn->{results}{firm}{results}->[$i]->{whs}{workhours}{$day}{'working_hours-0'}{to},
            $jsn->{results}{firm}{results}->[$i]->{whs}{workhours}{$day}{'working_hours-1'}{from});
        }
        else
        {
          if($jsn->{results}{firm}{results}->[$i]->{whs}{workhours}{$day}{'working_hours-0'})
          {
            $time = sprintf('%s-%s, Выходные: ',
              $jsn->{results}{firm}{results}->[$i]->{whs}{workhours}{$day}{'working_hours-0'}{from},
              $jsn->{results}{firm}{results}->[$i]->{whs}{workhours}{$day}{'working_hours-0'}{to});
          }
          else
          {
            $time = sprintf('%s-%s, Выходные: ',
              $jsn->{results}{firm}{results}->[$i]->{whs}{workhours}{$day}{'working_hours-1'}{from},
              $jsn->{results}{firm}{results}->[$i]->{whs}{workhours}{$day}{'working_hours-1'}{to});
          }
        }
      }
    }

		foreach my $d ( keys %wk )
    {
      next if $wk{$d}[1];
      $time .= " $wk{$d}[0]";
    }
    
    $db{$name}{whs} = $time;
    
		foreach my $k (keys %{$jsn->{results}{firm}{results}->[$i]->{contacts}[0]->{contacts}})
		{
			$k =~ /^(\w+).\d/;
			push @{$db{$name}{$1}}, $jsn->{results}{firm}{results}->[$i]->{contacts}[0]->{contacts}->{$k}->{rawdata};
		}
	}
	return ($$jsn{response_code}, \%db);
}
####################################################################################################
main:
{
  $|++;
  $/ = "\r\n" if $^O !~ /linux/;
	# урл для запроса
	my $url_temp = 'http://catalog.api.2gis.ru/advanced?criteria={"what":{"id":"##ID##","type":"rubric","scope":"full"},"types":["firm"],"sort":"relevance","page":##PAGE##,"filters":{"project_id":"##CITY##"},"magic":{"0":"advertising","geom2cache":"org_mini"}}&output=jsonp&key=ruokep8523&version=1.3&callback=DG.Utils.Ajax.callback.dga_2';
	# выбор города
	for(0..$#city_list)
	{
		my $tmp = $city_list[$_];
		$tmp =~ s/:\d+$//;
		printf("(%d) %s\n", $_, $tmp);
	}
	print "\n[?] Выберите город: ";
	chomp(my $aid = <STDIN>);
	#
	my ($city, $cid) = split/:/, $city_list[$aid];
	print("\n[?] Выберите файл-результат: ");
	chomp(my $save_as = <STDIN>);
  $save_as .= '.csv' if $save_as !~ /csv$/;
  #
  print qq~\n[+] Файл результата "$save_as" , город "$city"\n~;
	# получить рубрики
  print "[+] Получение рубрик...\n";
  my $rubs = get_rubs( $cid );
  # парсинг ГггГ
  my $count_rub = 1;
  open FH, ">$save_as" or die $!;
  # заголовок
  print FH encode('cp1251', join(';', @header)."\n");
  #open WH, ">.$save_as" or die $!;
  print "[+] Парсинг братан...\n";
  foreach my $rid ( keys %$rubs )
  {
    my $count_srub = 1;
    foreach my $srub ( @{$$rubs{$rid}{rubs}})
    {
      my ( $srub_id, $srub_name ) = ( $$srub[0], $$srub[1] );
      my ( $page, @pages ) = ( 1, $url_temp );

      while( @pages )
      {
        my $lnk = shift @pages;
        $lnk =~ s/##ID##/$srub_id/;
        $lnk =~ s/##CITY##/$cid/;
        $lnk =~ s/##PAGE##/$page/;
        # делаем запрос
        my ( $rcode, $ref ) = srub_parsing( get( $lnk ) );

        if( $rcode == 200 )
        {
          foreach my $n ( keys %$ref )
          {
            my @el = ($n, $$rubs{$rid}{name}, $srub_name);
            push @el, $ref->{$n}{addr}   ? $ref->{$n}{addr} : '';
            push @el, $ref->{$n}{info}   ? $ref->{$n}{info} : '';
            push @el, $ref->{$n}{phone}  ? join(",", @{$ref->{$n}{phone}}) : '';
            push @el, $ref->{$n}{fax}    ? join(",", @{$ref->{$n}{fax}}) : '';
            push @el, $ref->{$n}{whs}    ? $ref->{$n}{whs} : '';
            push @el, $ref->{$n}{web}    ? join(",", @{$ref->{$n}{web}}) : '';
            push @el, $ref->{$n}{email}  ? join(",", @{$ref->{$n}{email}}) : '';
            push @el, $ref->{$n}{lat}    ? $ref->{$n}{lat} : '';
            push @el, $ref->{$n}{lon}    ? $ref->{$n}{lon} : '';
            push @el, $ref->{$n}{city}   ? $ref->{$n}{city} : '';
            
						# Перевод с utf8 -> cp1251
            for(0..$#el)
            {
              $el[$_] =~ s/;/,/g;
            }
						print FH Encode::encode('cp1251', join(';', @el));
            print FH "\n";
          }
          push @pages, $url_temp;
          print "[+] Рубрик $count_rub/".scalar(keys %$rubs)." подрубрик $count_srub/".scalar(@{$$rubs{$rid}{rubs}})." страница $page\n";
          $page++;
        }
        else
        {
          my $msg = $lnk;
          $lnk .= "\t$@" if $@;
          #print WH $msg."\n";
          last;
        }
      }
      $count_srub++;
    }
    $count_rub++;
  }
  #close WH;
  close FH;
  
  print <<EOF;
[+] Работа окончена, с уважением ваш парсер $0 :-)

 +-------------------------------------------------------+
[ 2gis onliner parser 0.1.4 / 18.08.2012                  ]
 +-------------------------------------------------------+
EOF
};

# eof

__DATA__
0JDQu9C80LDRgtGLOjY3LNCQ0YDRhdCw0L3Qs9C10LvRjNGB0Lo6NDks0JDRgdGC0YDQsNGF0LDQvdGMOjgs0JHQsNGA0L3QsNGD0Ls6NCzQkdC10LvQs9C+0YDQvtC0OjQ2LNCR0LjQudGB0Lo6MjAs0JHQu9Cw0LPQvtCy0LXRidC10L3RgdC6OjUyLNCR0YDQsNGC0YHQujo1MSzQkdGA0Y/QvdGB0Lo6NjIs0JLQu9Cw0LTQuNCy0L7RgdGC0L7QujoyNSzQktC70LDQtNC40LzQuNGAOjU5LNCS0L7Qu9Cz0L7Qs9GA0LDQtDozMyzQktC+0YDQvtC90LXQtjozMSzQk9C+0YDQvdC+LdCQ0LvRgtCw0LnRgdC6OjI3LNCV0LrQsNGC0LXRgNC40L3QsdGD0YDQszo5LNCY0LLQsNC90L7QstC+OjY1LNCY0LbQtdCy0YHQujo0MSzQmNGA0LrRg9GC0YHQujoxMSzQmdC+0YjQutCw0YAt0J7Qu9CwOjcwLNCa0LDQt9Cw0L3RjDoyMSzQmtCw0LvQuNC90LjQvdCz0YDQsNC0OjQwLNCa0LDQu9GD0LPQsDo2MSzQmtC10LzQtdGA0L7QstC+OjUs0JrQuNGA0L7Qsjo1OCzQmtC+0YHRgtGA0L7QvNCwOjM0LNCa0YDQsNGB0L3QvtC00LDRgDoyMyzQmtGA0LDRgdC90L7Rj9GA0YHQujo3LNCa0YPRgNCz0LDQvToxMCzQm9C40L/QtdGG0Lo6NTYs0JzQsNCz0L3QuNGC0L7Qs9C+0YDRgdC6OjI2LNCc0L7RgdC60LLQsDozMizQndCw0LHQtdGA0LXQttC90YvQtSDQp9C10LvQvdGLOjI5LNCd0LjQttC90LXQstCw0YDRgtC+0LLRgdC6OjEyLNCd0LjQttC90LjQuSDQndC+0LLQs9C+0YDQvtC0OjE5LNCd0LjQttC90LjQuSDQotCw0LPQuNC7OjQ1LNCd0L7QstC+0LrRg9C30L3QtdGG0Lo6NizQndC+0LLQvtGB0LjQsdC40YDRgdC6OjEs0J7QtNC10YHRgdCwOjE0LNCe0LzRgdC6OjIs0J7RgNC10L3QsdGD0YDQszo0OCzQn9C10L3Qt9CwOjQyLNCf0LXRgNC80Yw6MTYs0KDQvtGB0YLQvtCyLdC90LAt0JTQvtC90YM6MjQs0KDRj9C30LDQvdGMOjQ0LNCh0LDQvNCw0YDQsDoxOCzQodCw0L3QutGCLdCf0LXRgtC10YDQsdGD0YDQszozOCzQodCw0YDQsNGC0L7Qsjo0MyzQodC+0YfQuDozMCzQodGC0LDQstGA0L7Qv9C+0LvRjDo1NyzQodGC0LDRgNGL0Lkg0J7RgdC60L7Quzo2MCzQodGC0LXRgNC70LjRgtCw0LzQsNC6OjU0LNCh0YPRgNCz0YPRgjozOSzQodGL0LrRgtGL0LLQutCw0YA6NzIs0KLQstC10YDRjDo0NyzQotC+0LvRjNGP0YLRgtC4OjIyLNCi0L7QvNGB0Lo6MyzQotGD0LvQsDozNizQotGO0LzQtdC90Yw6MTMs0KPQu9Cw0L0t0KPQtNGNOjM3LNCj0LvRjNGP0L3QvtCy0YHQujo1NSzQo9GE0LA6MTcs0KXQsNCx0LDRgNC+0LLRgdC6OjM1LNCn0LXQsdC+0LrRgdCw0YDRizo1MyzQp9C10LvRj9Cx0LjQvdGB0Lo6MTUs0KfQuNGC0LA6NjQs0K/QutGD0YLRgdC6OjUwLNCv0YDQvtGB0LvQsNCy0LvRjDoyOCzQkNCx0LDQutCw0L06Njks0JLQtdC70LjQutC40Lkg0J3QvtCy0LPQvtGA0L7QtDo3NyzQktC+0LvQvtCz0LTQsDo3OCzQk9C+0YDQvdGL0Lkg0JDQu9GC0LDQuToyNyzQmtGD0YDRgdC6OjczLNCd0L7QstC+0YDQvtGB0YHQuNC50YHQujo3NCzQodC80L7Qu9C10L3RgdC6OjYz
